<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* posts routes */

Route::get('/', 'PostsController@index')->name('posts.index');
Route::get('/post/show/{id}', 'PostsController@show')->name('posts.show');
Route::get('/post/create', 'PostsController@create')->name('posts.create');
Route::post('/post/store', 'PostsController@store')->name('posts.store');
Route::get('/post/edit/{id}', 'PostsController@edit')->name('posts.edit');
Route::put('/post/update/{id}', 'PostsController@update')->name('posts.update');
Route::delete('/post/destroy/{id}', 'PostsController@destroy')->name('posts.destroy');

Route::post('/post/comment/add/{id}', 'PostsController@addComment')->name('posts.comment.add');

/* categories routes */

Route::get('/categories', 'CategoriesController@index')->name('category.index');
Route::get('/category/show/{id}', 'CategoriesController@show')->name('category.show');
Route::get('/category/create', 'CategoriesController@create')->name('category.create');
Route::post('/category/store', 'CategoriesController@store')->name('category.store');
Route::get('/category/edit/{id}', 'CategoriesController@edit')->name('category.edit');
Route::put('/category/update/{id}', 'CategoriesController@update')->name('category.update');
Route::delete('/category/destroy/{id}', 'CategoriesController@destroy')->name('category.destroy');

Route::post('/category/comment/add/{id}', 'CategoriesController@addComment')->name('category.comment.add');
Route::delete('/file/delete/{id}', 'PostsController@fileDelete')->name('file.delete');
