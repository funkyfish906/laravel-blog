function addComment(id, item) {

    form = $('#add-comment-form');
    formData = form.serializeArray();
    token = formData._token;

    $.ajax({
        method: "POST",
        data: formData,
        url: "/" + item + "/comment/add/" + id,
        success: function (data) {

            if ($.isEmptyObject(data.error)) {
                var html = '<div class="comment"><h5>' + data.author + '</h5><p>' + data.content + '</p></div>';
                $("#comments").append(html);
                $('#add-comment-form')[0].reset();
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display', 'none');

            } else {

                $(".print-error-msg").css('display', 'block');

                $.each(data, function (key, value) {
                    $(".print-error-msg").find("ul").append('<li class="error">' + value + '</li>');
                });

            }
        }
    });
    return false;
}

function fileDelete(id) {
    formData = form.serializeArray();
    token = formData._token;
    $.ajax({
        method: "DELETE",
        url: "/file/delete/" + id,
        data: token,
        success: function (data) {
            $('#file').html('');
        }
    })
}






