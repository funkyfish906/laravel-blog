@extends('layouts.master')

@section('content')

<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">{{ trans('templates.category_list') }}</h1>
        <p class="add-item">{!! link_to_route('category.create', trans('templates.category_add_new'), null, array('class' => 'glyphicon glyphicon-plus')) !!}</p>
    </div>
</div>

@if ($categories->count())
    @foreach ($categories as $category)
        <div class="blog-post">
            <h2 class="blog-post-title">
                {!! link_to_route('category.show', $category->name, array($category->id)) !!}
            </h2>
                
            {{ $category->description }}
            <p class="blog-post-meta">
                {!! link_to_route('category.edit', trans('templates.edit'), array($category->id), array('class' => 'glyphicon glyphicon-pencil')) !!}
                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("templates.are_you_sure")."');",  'route' => array('category.destroy', $category->id))) !!}
                {!! Form::submit(trans('templates.delete'), array('class' => 'glyphicon glyphicon-remove')) !!}
                {!! Form::close() !!}
            </p>
           
        </div>
    @endforeach
@else
    {{ trans('templates.no_entries_found') }}
@endif

@endsection