@extends('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">{{ trans('templates.category_edit')}}</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
      </div>
</div>
<div class="blog-post">

    {!! Form::model($category, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'put', 'route' => array('category.update', $category->id))) !!}

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('name', trans('templates.category_name'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12">
            {!! Form::text('name', old('name',$category->name), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('description', trans('templates.category_description'), array('class'=>'label label-default')) !!}
         </div>
        <div class="col-sm-12">
            {!! Form::textarea('description', old('description', $category->description), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
          {!! Form::submit(trans('templates.update'), array('class' => 'btn btn-primary')) !!}
          {!! link_to_route('category.index', trans('templates.cancel'), null, array('class' => 'btn btn-default')) !!}
        </div>
    </div>

{!! Form::close() !!}

</div>

@endsection