@extends('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">{{ trans('templates.category_create')}}</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
      </div>
</div>
<div class="blog-post">

    {!! Form::open(array('route' => 'category.store', 'method' => 'post', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('name', trans('templates.category_name'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('description', trans('templates.category_description'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12">
            {!! Form::textarea('description', old('content'), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12s">
          {!! Form::submit( trans('templates.create') , array('class' => 'btn btn-primary')) !!}
        </div>
    </div>

{!! Form::close() !!}

</div>

@endsection