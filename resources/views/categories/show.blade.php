@extends('layouts.master')

@section('content')


    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">{{ $category->name}} </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="blog-post">

        <div class="col-sm-12">
            {!! $category->description !!}
        </div>

        <div class="col-sm-12 comments" id="comments">
            <h3>{{trans('templates.comments')}}</h3>
            @if ($comments->count())
                @foreach ($comments as $comment)
                    <div class="comment">
                        <h5>{{$comment->author }}</h5>
                        <p>{{$comment->content }}</p>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="col-sm-12">
            <div class="print-error-msg alert alert-danger">
                <ul>

                </ul>
            </div>
            <div class="row">
                {!! Form::open(array('method' => 'post', 'id' => 'add-comment-form', 'onsubmit' => 'return addComment('.$category->id.', "category")')) !!}
                <div class="form-group">
                    <div class="col-sm-12">
                        <h6>{{trans('templates.add_comment')}}</h6>
                    </div>
                    <div class="col-sm-12">
                        {!! Form::label('name', trans('templates.author'), array('class'=>'label label-default')) !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::text('author', old('name'), array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-sm-12">
                        {!! Form::label('name', trans('templates.comment'), array('class'=>'label label-default')) !!}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::textarea('content', old('content'), array('class'=>'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        {!! Form::submit(trans('templates.add_comment'), array('class' => 'btn btn-xs btn-danger')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-sm-12 comments" id="comments">
                <h3>Пости</h3>
                @if ($posts->count())
                    @foreach ($posts as $item)
                        <div class="comment">
                            <h5>{{$item->name }}</h5>
                            <p>{{$item->content }}</p>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection