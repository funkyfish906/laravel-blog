<div class="col-sm-3 offset-sm-1 blog-sidebar">
    <div class="sidebar-module">
        <h4>{!! link_to_route('category.index', trans('templates.categories')) !!}</h4>
        <ol class="list-unstyled">
            @foreach ($lastCategory as $item)
                <li>{!! link_to_route('category.show', $item->name, array($item->id)) !!}</li>
            @endforeach
        </ol>
        <h4> {{ trans('templates.user_session') }}</h4>
        <ul>
            @foreach($userCount as $item)
                <li> {{$item->user_agent.': '.$item->count}}</li>
            @endforeach
        </ul>
    </div>
</div>
