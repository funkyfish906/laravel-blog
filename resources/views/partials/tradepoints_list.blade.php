<div class="list-group w-100">
	
	<a class="list-group-item list-group-item-action border-0 rounded-0" href="{{ route('admin.tradepoints.create') }}"><i class="fa fa-plus-circle add-tradepoint-i" aria-hidden="true"></i> {{ trans('quickadmin::admin.tradepoints-list-add') }}</a>
	
	@foreach (App\TradePoints::getTradepointsList() as $tradepoints_list)
	<?php if((isset($tradepoints->id))&&($tradepoints_list->id == $tradepoints->id)) {$active = 'active';}else{$active = '';} ?>
	{!! link_to_route(config('quickadmin.route').'.tradepoints.edit', $tradepoints_list->name, array($tradepoints_list->id), array('class' => 'list-group-item list-group-item-action border-0 rounded-0 '.$active.'')) !!}
	@endforeach 
	
</div>