@include('partials.header')
@include('partials.topbar')

<div class="container">
	<div class="row"> 
	        
		<div class="col-sm-8 blog-main">
		    @if (Session::has('message'))
		      <div class="alert alert-success">
		            <p>{{ Session::get('message') }}</p>
		       </div>
		    @endif
		    @yield('content')
		</div>

		@include('partials.sidebar')

	</div>
</div>
<div class="scroll-to-top"
     style="display: none;">
    <i class="fa fa-arrow-up"></i>
</div>

@include('partials.footer')


