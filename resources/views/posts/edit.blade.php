@extends('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">{{ trans('templates.posts_edit') }}</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
      </div>
</div>
<div class="blog-post">

    {!! Form::model($post, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'put', 'route' => array('posts.update', $post->id))) !!}

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('name', trans('templates.post_name'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12">
            {!! Form::text('name', old('name',$post->name), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('content', trans('templates.post_content'), array('class'=>'label label-default')) !!}
         </div>
        <div class="col-sm-12">
            {!! Form::textarea('content', old('content', $post->content), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('file', trans('templates.file'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12" class="file-input">
            @if (empty($post->file))
            {!! Form::file('content', old('file'), array('class'=>'form-control')) !!}
            @else
                <div id="file">
                {{$post->file}}
                    <span onclick="fileDelete({{$post->id}})">{{trans('templates.delete')}}</span>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('categories', 'Категории', array('class'=>'col-12 col-form-label')) !!}
         <div class="col-12 d-flex justify-content-between flex-wrap">

            @foreach($categories as $item)
                <div class="checkbox">
                    <label class="custom-control custom-checkbox">


                        @if (in_array($item->id, $categoriesList))
                            {!! Form::checkbox('categories[]', $item->id, true, array('class'=>'custom-control-input')) !!}
                        @else
                            {!! Form::checkbox('categories[]', $item->id, false, array('class'=>'custom-control-input')) !!}
                        @endif
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">{{$item->name}}</span>
                    </label>
                </div>
            @endforeach
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::submit(trans('templates.update'), array('class' => 'btn btn-primary')) !!}
            {!! link_to_route('posts.index', trans('templates.cancel'), null, array('class' => 'btn btn-default')) !!}
        </div>
    </div>

{!! Form::close() !!}

</div>

@endsection