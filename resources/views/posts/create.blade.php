@extends('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">{{ trans('templates.post_create') }}</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
      </div>
</div>
<div class="blog-post">

    {!! Form::open(array('files' => true,'route' => 'posts.store', 'method' => 'post', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('name', trans('templates.post_name'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('content', trans('templates.post_content'), array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12">
            {!! Form::textarea('content', old('content'), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            Категории
            @foreach($categories as $key=>$item)
                <div class="checkbox">
                    <label>
                         {!! Form::checkbox('categories[]', $key, false) !!}
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        {{$item}}
                    </label>
                </div>
            @endforeach
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('file', 'File', array('class'=>'label label-default')) !!}
        </div>
        <div class="col-sm-12">
            {!! Form::file('file', old('file'), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12s">
          {!! Form::submit( trans('templates.create') , array('class' => 'btn btn-primary')) !!}
        </div>
    </div>

{!! Form::close() !!}

</div>

@endsection