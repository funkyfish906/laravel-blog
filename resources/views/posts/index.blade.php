@extends('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">{{ trans('templates.posts_list') }}</h1>
        <p class="float-right">{!! link_to_route('posts.create', trans('templates.posts_add_new') , null, array('class' => 'glyphicon glyphicon-plus')) !!}</p>
      </div>
</div>

@if ($posts->count())
    @foreach ($posts as $row)
        <div class="blog-post">
            <h2 class="blog-post-title">
                {!! link_to_route('posts.show', $row->name, array($row->id)) !!}
            </h2>
                
            {{ $row->content }}
            <p class="blog-post-meta">
                {!! link_to_route('posts.edit', trans('templates.edit'), array($row->id), array('class' => 'glyphicon glyphicon-pencil')) !!}
                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("templates.are_you_sure")."');",  'route' => array('posts.destroy', $row->id))) !!}
                {!! Form::submit(trans('templates.delete'), array('class' => 'glyphicon glyphicon-remove')) !!}
                {!! Form::close() !!}
            </p>
           
        </div>
    @endforeach
@else
    {{ trans('templates.no_entries_found') }}
@endif

@endsection