<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryComment extends Model
{

    protected $fillable = [
        'post_id',
        'author',
        'content'
    ];
}
