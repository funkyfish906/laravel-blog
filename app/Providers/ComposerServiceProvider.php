<?php

namespace App\Providers;

use App\Category;
use App\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('*', function ($view) {
            $view->with('lastCategory', Category::limit(10)->orderByDesc('id')->get());
            $view->with('userCount', Session::selectRaw('count(id) as count, user_agent')->groupBy('user_agent')->get());
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}