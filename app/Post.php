<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	public $timestamps = false;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'content', 'file',
    ];

    public function comments(){
        return $this->hasMany(PostComment::class);
    }

    public function category(){
        return $this->belongsToMany(Category::class, 'post_categories', 'post_id', 'category_id');
    }
}
