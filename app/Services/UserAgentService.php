<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.10.2017
 * Time: 23:52
 */

namespace App\Services;

class UserAgentService
{

    //protected $userAgent;

    public function getUserAgent($userAgent)
    {
        if (preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent)) {
            $name = 'Internet Explorer';

        } elseif (preg_match('/Firefox/i', $userAgent)) {
            $name = 'Mozilla Firefox';

        } elseif (preg_match('/Chrome/i', $userAgent)) {
            $name = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $userAgent)) {
            $name = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $userAgent)) {
            $name = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $userAgent)) {
            $name = 'Netscape';
            $ub = "Netscape";
        }

        return $name;
    }

}