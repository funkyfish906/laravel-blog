<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\PostComment;
use App\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    public function index()
    {
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::pluck('name', 'id');

        return view('posts.create', compact('categories'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name'    => 'required',
            'content' => 'required',
            'file'    => 'nullable|max:2048',
        ]);

        $post = new Post;
        $data = $request->except('file');

        if ($request->file('file') != null) {

            $imageName = str_random(15) . '.' .
                $request->file('file')->getClientOriginalExtension();

            $request->file('file')->move(
                base_path() . '/public/images/', $imageName
            );

            $data['file'] = $imageName;
        }

        $post->fill($data);
        $post->save();

        if (isset($data['categories'])) {
            foreach ($data['categories'] as $category) {
                $postcategory = new PostCategory;
                $postcategory->create(['post_id' => $post->id, 'category_id' => $category]);
            }
        }

        return redirect()->route('posts.index')->withMessage(trans('templates.posts-controller-successfully_created'));
    }

    public function edit($id)
    {
        $categories = Category::select('id', 'name')->get();
        $post = Post::findOrFail($id);

        $categoriesList = $post->category->pluck('id')->toArray();

        return view('posts.edit', compact('post', 'categories', 'categoriesList'));
    }

    public function update(Request $request, $id)
    {
        $categoriesList = $request->categories;
        $data = $request->all();

        PostCategory::where('post_id', $id)->delete();
        foreach ($categoriesList as $category) {
            $postCategory = new PostCategory;
            $postCategory->create(['post_id' => $id, 'category_id' => $category]);
        }

        if ($request->file('file') != null) {

            $imageName = str_random(15) . '.' .
                $request->file('file')->getClientOriginalExtension();

            $request->file('file')->move(
                base_path() . '/public/images/', $imageName
            );

            $data['file'] = $imageName;
        }

        Post::findOrFail($id)->update($data);

        return redirect()->route('posts.index')->withMessage(trans('templates.posts-controller-successfully_updated'));
    }

    public function show(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $comments = $post->comments;
        $category = $post->category()->select('post_categories.*')->get();

        return view('posts.show', compact('post', 'comments', 'category'));
    }

    public function destroy($id)
    {
        Post::findOrFail($id)->delete();

        return redirect()->route('posts.index')->withMessage(trans('templates.posts-controller-successfully_deleted'));
    }

    public function addComment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'author'  => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $data = $request->all();
        $data['post_id'] = $id;
        $comment = new PostComment;
        $comment->fill($data);
        $comment->save();
        $dt = $comment->created_at->toDateTimeString();

        return response()->json(['author' => $data['author'], 'content' => $data['content'], 'created_at' => $dt]);
    }

    public function fileDelete($id)
    {
        dd($id);
        Post::findOrFail($id)->update(['file' => null]);

        return redirect()->route('posts.edit',
            ['id' => $id])->withMessage(trans('templates.file-successfully_deleted'));
    }
}

