<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryComment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Category::all();

        return view('categories.index', compact('categories'));
    }

    public function create()
    {

        return view('categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
        ]);
        $category = new Category();
        $category->create($request->only('name', 'description'));

        return redirect()->route('category.index')->withMessage(trans('templates.category-controller-successfully_created'));
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        Category::findOrFail($id)->update($request->all());

        return redirect()->route('category.index')->withMessage(trans('templates.category-controller-successfully_updated'));

    }

    public function show($id)
    {
        $category = Category::findOrFail($id);

        $comments = $category->comments;
        $posts =  $category->posts;


        return view('categories.show', compact('category', 'comments','posts'));
    }

    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return redirect()->route('category.index')->withMessage(trans('templates.category-controller-successfully_deleted'));
    }

    public function addComment(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'author'  => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $data = $request->all();

        $comment = new CategoryComment;
        $comment->category_id = $id;
        $comment->fill($data);
        $comment->save();
        $dt = $comment->created_at->toDateTimeString();

        return response()->json(['author' => $data['author'], 'content' => $data['content'], 'created_at' => $dt]);
    }
}