<?php

namespace App\Http\Middleware;

use App\Session;
use App\Services\UserAgentService;

class UserSession
{
    protected $userAgent;

    public function __construct(UserAgentService $userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function handle($request, \Closure $next)
    {
        return $next($request);
    }

    /**
     * @param $request
     * @param $response
     */
    public function terminate($request)
    {

        $ip = $request->ip();
        $userAgent = $request->server('HTTP_USER_AGENT');
        $agent = $this->userAgent->getUserAgent($userAgent);

        Session::firstOrCreate(['user_ip' => $ip, 'user_agent' => $agent]);

    }
}