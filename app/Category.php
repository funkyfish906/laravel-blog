<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categories';
	public $timestamps = false;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
       'id', 'name', 'description',
    ];


    public function comments(){
        return $this->hasMany(CategoryComment::class);
    }

    public function posts(){
        return $this->belongsToMany(Post::class,'post_categories', 'post_id', 'category_id');
    }

}
